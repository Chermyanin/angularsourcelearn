﻿'use sctrict';
var app = angular.module("app",[]);
app.factory('loadService',['$http','$q',function($http,$q) {
    return {
        load:function() {
            var deferred = $q.defer();
            $http.get('/app/data.json').success(function (res){
                deferred.resolve(res);
            });
            return deferred.promise;
        }
    };
}]);
app.controller("appCtrl",['$scope','loadService',function($scope,loadService) {
    $scope.hello = "hello";
    $scope.load = function() {
        loadService.load().then(function(res) {
            $scope.loaded = res;
        });
    };
}]);
